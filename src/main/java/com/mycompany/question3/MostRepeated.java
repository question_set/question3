package com.mycompany.question3;

import java.util.ArrayList;
import java.util.List;

/**
 * MostRepeated is a main class uses Functions class's findMostRepeated method
 *
 * @author fatih
 */
public class MostRepeated 
{
    public static void main( String[] args )
    {
         List<String> words = new ArrayList<String>();

        words.add("apple");
        words.add("pie");
        words.add("apple");
        words.add("red");
        words.add("red");
        words.add("red");
        
        System.out.println(Functions.findMostRepeated(words));
    }
}
