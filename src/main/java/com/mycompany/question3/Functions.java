package com.mycompany.question3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Functions class have a method findMostRepeated
 *
 * @author fatih
 */

public class Functions {

    /**
     * findMostRepeated method has a List-String- parameter.
     * Method counts for every words in list and collects using Map.
     * Finally searches word that has maximum count in map and
     * return it.
     * @param words- a list-String-
     * @return String- is the most repeated word
     */

    public static String findMostRepeated(List<String> words) {

        Map<String, Integer> counts = new HashMap<String, Integer>();
        for (String s : words) {
            if (counts.containsKey(s)) {
                counts.put(s, counts.get(s) + 1);
            } else {
                counts.put(s, 1);
            }
        }

        int maxValue = 0;
        String maxString = "";
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            if (maxValue < entry.getValue()) {
                maxString = entry.getKey();
                maxValue = entry.getValue();
            }
        }
        return maxString;

    }

}
