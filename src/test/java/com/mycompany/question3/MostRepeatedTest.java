package com.mycompany.question3;

import java.util.ArrayList;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MostRepeatedTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MostRepeatedTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MostRepeatedTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        
        //test content is being created
        List<String> testWords = new ArrayList<String>();
        testWords.add("aaa");
        testWords.add("bbb");
        testWords.add("aaa");
        testWords.add("ccc");
        testWords.add("aaa");
        testWords.add("ddd");
        //the method to be tested is being running
        String result=Functions.findMostRepeated(testWords);
        //expected result
        String expectedResult = "aaa";  
        //testing
        assertEquals(expectedResult, result);
    }
}
